// Package que is essentially bloilerplate for interacting with rabbitmq
package que

import (
	"github.com/streadway/amqp"
)

var (
	url        = "amqp://guest:guest@rabbit/"
	connection *amqp.Connection
	err        error
)

func init() {
	// Connect to the rabbitMQ instance
	connection, err = amqp.Dial(url)
	if err != nil {
		panic("could not establish connection with RabbitMQ:" + err.Error())
	}
}

func SetupQue() {
	channel := GetChannel()

	err = channel.ExchangeDeclare("events", "topic", true, false, false, false, nil)
	if err != nil {
		panic(err)
	}

	_, err = channel.QueueDeclare("jobs", true, false, false, true, nil)
	if err != nil {
		panic("error declaring the queue: " + err.Error())
	}

	err = channel.QueueBind("jobs", "#", "events", false, nil)
	if err != nil {
		panic("error binding to the queue: " + err.Error())
	}
	channel.Qos(1, 0, true)
}

func GetChannel() *amqp.Channel {
	channel, err := connection.Channel()
	if err != nil {
		panic("could not open RabbitMQ channel:" + err.Error())
	}

	return channel
}

func PublishMsg(b []byte) (err error) {
	message := amqp.Publishing{
		Body: b,
	}

	channel := GetChannel()
	err = channel.Publish("events", "jobs", true, false, message)
	return
}
