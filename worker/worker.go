package worker

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	"gitlab.com/tokamak-git/bcs/job"
	"gitlab.com/tokamak-git/bcs/que"
	"gitlab.com/tokamak-git/bcs/store"
)

var workerName string

func init() {
	workerName = os.Getenv("WORKER_NAME")
}

// Run is our worker
// it connects to a que and pulls data
//
// order of execution
// connect to que
// read from que in an endless loop
// check if the object has run in the last 5 mins
//  if not store it in cache for reference
//
// if it has then ack the msg so that the value is dropped from the que
// if not then the execution begins
//
// if no error, the status of the job is updated to `fin` // else its set as timed_out
// timed_out jobs can be put back into que this is being done by a different
// process
//
// if a job is dropped due to restarts the message will not be acked, thus the
// que will push it back to a worker node.
func Run() {
	channel := que.GetChannel()
	msgs, err := channel.Consume("jobs", "", false, false, false, false, nil)
	if err != nil {
		panic("error consuming the queue: " + err.Error())
	}

	for msg := range msgs {
		fmt.Println("message received: " + string(msg.Body))
		var j job.J
		json.Unmarshal(msg.Body, &j)
		found, err := j.CheckTimeSinceLastAttempt(store.GetClient()) // check if job is valid
		if err != nil {
			panic(err)
		}
		if !found {
			err = j.SetProcessedTime(store.GetClient(), job.Timeout) // set in cache
			if err != nil {
				panic(err)
			}
			j.UpdateStatus("processing")
			j.UpdateWorkerDetails(workerName)
			err = j.Process(job.Timeout)
			if err == nil {
				j.UpdateStatus("fin")
			} else {
				j.UpdateStatus("timed_out")
				log.Println("[ERROR]", err.Error())
				msg.Nack(false, true)
			}
		}
		msg.Ack(false)
	}
}
