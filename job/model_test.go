package job

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/tokamak-git/bcs/store"
)

func TestProcess(t *testing.T) {
	tc := []struct {
		J
		timeout int
		expOut  interface{}
		msg     string
	}{
		{
			J: J{
				SleepTime: 100,
			},
			timeout: 1,
			expOut:  "job timed out",
			msg:     "testing timeout exit",
		},
		{
			J: J{
				SleepTime: 1,
			},
			timeout: 10,
			expOut:  nil,
			msg:     "successfull execution of processing",
		},
		// NOTE to self: a more elegent solution for this edge case would be interesting
		{
			J: J{
				SleepTime: 1,
			},
			timeout: 1,
			expOut:  nil,
			msg:     "successfull execution of processing",
		},
	}
	for i, c := range tc {
		out := c.Process(c.timeout)
		if out == nil {
			assert.Equal(t, c.expOut, out, fmt.Sprintf("i: %v, msg: %v", i, c.msg))
			continue
		}
		assert.Equal(t, c.expOut, out.Error(), fmt.Sprintf("i: %v, msg: %v", i, c.msg))
	}
}

func TestCheckTimeSinceLastAttempt(t *testing.T) {
	tc := []struct {
		J
		wait    int
		timeout int
		expOut  interface{}
		expErr  error
		msg     string
	}{
		{
			J: J{
				ObjID: 1,
			},
			timeout: 3,
			wait:    5,
			expOut:  false,
			expErr:  nil,
			msg:     "key should have expired",
		},
		{
			J: J{
				ObjID: 2,
			},
			timeout: 3,
			wait:    1,
			expOut:  true,
			expErr:  nil,
			msg:     "key should be present",
		},
	}
	conn := store.GetClient()
	for i, c := range tc {
		c.SetProcessedTime(conn, c.timeout)
		time.Sleep(time.Duration(c.wait) * time.Second)
		out, err := c.CheckTimeSinceLastAttempt(conn)
		assert.Equal(t, c.expOut, out, fmt.Sprintf("i: %v, msg: %v", i, c.msg))
		assert.Equal(t, c.expErr, err, fmt.Sprintf("i: %v, msg: %v", i, c.msg))
	}
}

func TestUpdateStatus(t *testing.T) {
	tc := struct {
		J
		status string
		msg    string
	}{
		J: J{
			ObjID:  999,
			Status: "testing",
		},
		status: "done",
		msg:    "testing updatation of status for job object",
	}
	store.DB.Create(&tc.J)
	tc.UpdateStatus(tc.status)

	store.DB.First(&tc.J, "obj_id = ?", tc.ObjID)
	assert.Equal(t, tc.status, tc.Status)
	store.DB.Delete(&tc.J)
}
