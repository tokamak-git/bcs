package job

import (
	"encoding/json"
	"time"

	"gitlab.com/tokamak-git/bcs/que"
	"gitlab.com/tokamak-git/bcs/store"
)

var (
	ticker = time.NewTicker(1 * time.Minute)

	SelInitInactiveJobs = func() {
		var js []J
		store.DB.Where("status IN ?", []string{"pending", "processing"}).Find(&js)
		publishMsg(js)
	}
)

// SelInactiveJobs selects all jobs that have timedout/foce stoped/etc
func SelInactiveJobs() {
	for _ = range ticker.C {
		var js []J
		store.DB.Where("status = ?", "timed_out").Find(&js)
		publishMsg(js)
	}
}

func publishMsg(js []J) {
	for _, j := range js {
		if found, _ := j.CheckTimeSinceLastAttempt(store.GetClient()); !found {
			b, err := json.Marshal(j)
			if err != nil {
				panic(err)
			}
			err = que.PublishMsg(b)
			if err != nil {
				panic(err)
			}
			j.UpdateStatus("in_que")
		}
	}
}
