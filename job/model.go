// Package job holds the primary application execution logic
// the J obj implements all the methods on itself thereby allowing for granular
// control over its behaviour
package job

import (
	"context"
	"errors"
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/go-redis/redis/v8"
	"gitlab.com/tokamak-git/bcs/rand"
	"gitlab.com/tokamak-git/bcs/store"
)

var (
	defaultTimeout int = 300
	Timeout        int
	err            error
)

func init() {
	Timeout, _ = strconv.Atoi(os.Getenv("DEFAULT_TIMEOUT"))
	err = store.DB.AutoMigrate(&J{}) // creates the table required for the J struct
	if err != nil {
		panic(err)
	}

}

// J the base job object
type J struct {
	ObjID      int       `json:"obj_id" gorm:"uniqueIndex"`
	JobID      int       `json:"job_id" gorm:"primaryKey"`
	Timestamp  time.Time `json:"timestamp"`
	Status     string    `json:"status"`
	SleepTime  int       `json:"sleep_time"`
	WorkerNode string    `json:"worker"`
}

// InitAndValidate used to expose the underlying implementation for initilizing
// the data and validating it for the rest server
func (j *J) InitAndValidate() (err error) {
	err = j.init()
	if err != nil {
		return err
	}
	return j.validate()
}

func (j *J) validate() (err error) {
	if j.ObjID <= 0 {
		return errors.New("Invalid obj_id")
	}
	return nil
}

func (j *J) init() (err error) {
	j.Timestamp = time.Now()
	j.Status = "pending"
	j.SleepTime = rand.GenRand()
	return nil
}

// UpdateStatus is used to interface with the db model so as to change the
// status in the store as per the current status of the job
func (j *J) UpdateStatus(status string) {
	store.DB.Model(&j).Update("status", status).Where("job_id = ?", j.JobID)
}

// Process the main processe execution logic, it generates 2 time.After channels
// basically a version of sleep that write to a channel after they have executed
// for a predefined interval of time
//
// Using the Using the timeout set as an env var we are able to customise this
// duration for an early exit
//
// NOTE if the timeout and sleeptime are equal, it would make sense to increase
// the sleeptime for the sake of consistency in tests
func (j *J) Process(timeout int) (err error) {
	if j.SleepTime == timeout { // so as to maintain sanity in tests
		timeout++
	}
	afterCH := time.After(time.Duration(j.SleepTime) * time.Second)
	timeoutCH := time.After(time.Duration(timeout) * time.Second)
	doneCh := make(chan bool)

	go func() {
		<-timeoutCH
		doneCh <- false
	}()
	go func() {
		<-afterCH
		doneCh <- true
	}()

	switch <-doneCh {
	case false:
		err = errors.New("job timed out")
	case true:
	}

	return
}

func (j *J) CheckTimeSinceLastAttempt(c *redis.Client) (found bool, err error) {
	_, err = c.Get(context.Background(), fmt.Sprintf("obj_%d", j.JobID)).Result()
	if err != nil {
		if err == redis.Nil {
			err = nil
		}
		return
	}
	found = true
	return
}

func (j *J) SetProcessedTime(c *redis.Client, timeout int) (err error) {
	err = c.SetEX(context.Background(), fmt.Sprintf("obj_%d", j.JobID), j.Timestamp.String(), time.Duration(timeout)*time.Second).Err()
	return
}

func (j *J) UpdateWorkerDetails(worker string) {
	store.DB.Model(&j).Update("worker_node", worker).Where("job_id = ?", j.JobID)
}
