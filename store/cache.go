// Package store contains boilerplate and connection configs for postgres and
// redis
package store

import (
	"github.com/go-redis/redis/v8"
)

var rdb = redis.NewClient(&redis.Options{
	Addr:     "redis:6379",
	Password: "", // no password set
	DB:       0,  // use default DB
})

func GetClient() *redis.Client {
	return rdb
}
