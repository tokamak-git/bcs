// Package handler is responsible for dealing with the rest interface
// it handler POST and GET functions
// Moreover it levarages the underlying model object to calculate/validate data
// on the fly
package handler

import (
	"encoding/json"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/tokamak-git/bcs/job"
	"gitlab.com/tokamak-git/bcs/que"
	"gitlab.com/tokamak-git/bcs/store"
)

type JobHandler struct {
	job.J
}

// UnmarshalJSON is used to unmarshal data into the underlying model
// it runs the basic initialization and validation of data on the model aswell
func (jh *JobHandler) UnmarshalJSON(b []byte) (err error) {
	err = json.Unmarshal(b, &jh.J)
	if err != nil {
		return err
	}
	return jh.InitAndValidate()
}

func (jh JobHandler) GetHandler(c *gin.Context) {
	jh = JobHandler{}
	jobID, _ := c.Params.Get("id")

	// lookup for job in store
	err := store.DB.First(&jh.J, "job_id = ?", jobID).Error
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// return data
	c.JSON(200, jh.J)
}

func (jh JobHandler) PostHandler(c *gin.Context) {
	jh = JobHandler{}
	// unmarshal data into struct
	err := c.ShouldBindJSON(&jh)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// store data into db
	err = store.DB.Create(&jh.J).Error
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// send data to que
	b, err := json.Marshal(jh.J)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	err = que.PublishMsg(b)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	// update db with que status
	jh.UpdateStatus("in_que")

	c.JSON(200, jh.J)
}
