package handler

import "github.com/gin-gonic/gin"

// Run initializes the rest server
func Run() {
	r := setupRouter()
	r.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")

}

func setupRouter() *gin.Engine {
	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		c.String(200, "pong")
	})
	jh := new(JobHandler)
	r.GET("/job/:id", func(c *gin.Context) {
		jh.GetHandler(c)
	})
	r.POST("/job", func(c *gin.Context) {
		jh.PostHandler(c)
	})

	return r
}
