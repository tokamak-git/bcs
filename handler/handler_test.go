package handler

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/tokamak-git/bcs/job"
	"gitlab.com/tokamak-git/bcs/store"
)

func TestPingRoute(t *testing.T) {
	router := setupRouter()

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/ping", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, "pong", w.Body.String())
}

func Test_loadMultiple(t *testing.T) {
	router := setupRouter()
	for i := 1000; i < 1100; i++ {
		w := httptest.NewRecorder()
		req, _ := http.NewRequest("POST", "/job", strings.NewReader(fmt.Sprintf(`{"obj_id": %d}`, i)))
		w.Header().Set("Content-Type", "application/json")
		router.ServeHTTP(w, req)
		assert.Equal(t, 200, w.Code, "testing valid obj_id insertion")
	}
}

func Test_requsetLifeCycle(t *testing.T) {
	router := setupRouter()

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/job", strings.NewReader(`{"obj_id": 11989}`))
	w.Header().Set("Content-Type", "application/json")
	router.ServeHTTP(w, req)
	assert.Equal(t, http.StatusOK, w.Code, "testing valid obj_id insertion")

	var j job.J
	store.DB.First(&j, "obj_id = 11989")
	assert.Contains(t, []string{"in_que", "processing"}, j.Status, "tesing post handler lifecycle")

	w = httptest.NewRecorder()
	req, _ = http.NewRequest("POST", "/job", strings.NewReader(`{"obj_id": 11989}`))
	w.Header().Set("Content-Type", "application/json")
	router.ServeHTTP(w, req)
	assert.Equal(t, http.StatusBadRequest, w.Code, "testing valid obj_id insertion")

	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", fmt.Sprintf("/job/%d", j.JobID), nil)
	router.ServeHTTP(w, req)
	assert.Equal(t, http.StatusOK, w.Code, "testing get valid obj_id")
}
