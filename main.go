// Package main runs the program
// it check for env var BCS_MODE
// if set to worker runs the service as a worker node
package main

import (
	"os"
	"sync"

	"gitlab.com/tokamak-git/bcs/handler"
	"gitlab.com/tokamak-git/bcs/job"
	"gitlab.com/tokamak-git/bcs/que"
	"gitlab.com/tokamak-git/bcs/worker"
)

var once sync.Once

func main() {
	mode := os.Getenv("BCS_MODE")
	switch mode {
	case "WORKER":
		worker.Run()
	default:
		que.SetupQue()
		once.Do(job.SelInitInactiveJobs)
		go job.SelInactiveJobs()
		handler.Run()
	}
}
