FROM golang:1.15-alpine

RUN apk --update --no-cache add bash curl postgresql-client gcc git g++ dpkg-dev libc-dev

# Set the Current Working Directory inside the container
WORKDIR $GOPATH/src/gitlab.com/tokamak-git/bcs

# Copy everything from the current directory to the PWD (Present Working Directory) inside the container
COPY . .

# Install the package
# RUN go install -v ./...

# This container exposes port 8080 to the outside world
EXPOSE 8080

# CMD ["bcs"]
CMD ["watcher"]
