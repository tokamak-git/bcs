# backend case study

### requirements

`docker`
`docker-compose`

### command

`docker-compose up --build`


Once initilized the multiple services are spun up and connected
to investigate `docker container ls -a`


## Endpoints

### service
GET 8080 /job/:id
POST 8080 /job `{"obj_id":1}`

### deps
rabbit-management: 15762
rabbit: 5762
redis: 6972
psql: 5432
