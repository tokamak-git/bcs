package rand_test

import (
	"testing"

	"gitlab.com/tokamak-git/bcs/rand"
)

func TestGenRand(t *testing.T) {
	for i := 0; i < 100000; i++ {
		v := rand.GenRand()
		if v < 15 || v > 40 {
			t.Error("Invalid random value", v)
		}
	}
}
