package rand

import (
	"math/rand"
	"time"
)

// GenRand generates a random number between 15 and 40
// the values are hard coded in order to avoid reading from a config file
// the ideal place for such values
func GenRand() int {
	rand.Seed(time.Now().UnixNano())
	min := 15
	max := 40
	return rand.Intn(max-min+1) + min
}
